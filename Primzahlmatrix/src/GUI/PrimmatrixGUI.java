package GUI;

import math.MyMath;
import math.PrimGenerator;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;

public class PrimmatrixGUI extends JFrame {

    public PrimmatrixGUI(long startzahl) {
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));

        JPanel grid = new JPanel();
        contentPane.add(grid, BorderLayout.CENTER);
        grid.setLayout(new GridLayout(0, 200, 0, 0));

        boolean[] zahlen = PrimGenerator.generatePrimNumbers(startzahl);
        long startzahlGerundet = MyMath.roundDownToLast10000(startzahl);

        for (int i = 0; i < zahlen.length; i++) {
            JPanel tmp = new JPanel();
            tmp.setToolTipText(String.valueOf(i + 1 + startzahlGerundet));
            tmp.setSize(5, 5);

            if (zahlen[i]) {
                tmp.setBackground(Color.RED);
            } else {
                tmp.setBackground(Color.WHITE);
            }

            tmp.setBorder(new LineBorder(new Color(200, 200, 200)));

            grid.add(tmp);
        }

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(250, 40, 1400, 1000);
        setTitle(startzahlGerundet + "-" + (startzahlGerundet + 40000));
        setVisible(true);
    }

    public static void main(String[] args) {
        PrimmatrixGUI primmatrixGUI = new PrimmatrixGUI(5000);
    }
}
