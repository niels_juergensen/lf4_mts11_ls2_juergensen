package test;

import math.MyMath;
import math.PrimGenerator;

public class MyMathTest {

    public static void main(String[] args) {
        long test_1 = 1;
        long test_2 = 2;
        long test_5011 = 5011;
        long test_9973 = 9973;
        long test_10000 = 10000;

        System.out.println(MyMath.isPrim(test_1));
        System.out.println(MyMath.isPrim(test_2));
        System.out.println(MyMath.isPrim(test_5011));
        System.out.println(MyMath.isPrim(test_9973));
        System.out.println(MyMath.isPrim(test_10000));
    }
}
