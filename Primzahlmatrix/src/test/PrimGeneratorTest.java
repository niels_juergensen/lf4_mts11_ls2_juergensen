package test;

import math.PrimGenerator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PrimGeneratorTest {

    boolean[] prim10000 = PrimGenerator.generatePrimNumbers();

    @Test
    public void primTest_1() {
        assertFalse(prim10000[0]);
    }

    @Test
    public void primTest_2() {
        assertTrue(prim10000[1]);
    }

    @Test
    public void primTest_3555() {
        assertFalse(prim10000[3554]);
    }

    @Test
    public void primTest_8269() {
        assertTrue(prim10000[8268]);
    }

    @Test
    public void primTest_10000() {
        assertFalse(prim10000[9999]);
    }

    @Test
    public void prim10000_outOfBounds_Test() {
        Exception exception = assertThrows(ArrayIndexOutOfBoundsException.class, () -> prim10000[10000] = true);
        assertEquals("Index 10000 out of bounds for length 10000", exception.getMessage());
    }

    @Test
    public void primTest_24414() {
        boolean[] prim30000 = PrimGenerator.generatePrimNumbers(24414);
        assertFalse(prim30000[4413]);
    }

    @Test
    public void primTest_29921() {
        boolean[] prim30000 = PrimGenerator.generatePrimNumbers(29921);
        assertTrue(prim30000[9920]);
    }

    @Test
    public void primTest_41076() {
        boolean[] prim50000 = PrimGenerator.generatePrimNumbers(41076);
        assertFalse(prim50000[1075]);
    }

    @Test
    public void primTest_43781() {
        boolean[] prim50000 = PrimGenerator.generatePrimNumbers(43781);
        assertTrue(prim50000[3780]);
    }

    @Test
    public void prim30000_outOfBounds_Test() {
        boolean[] prim30000 = PrimGenerator.generatePrimNumbers(24414);
        Exception exception = assertThrows(ArrayIndexOutOfBoundsException.class, () -> prim30000[10000] = true);
        assertEquals("Index 10000 out of bounds for length 10000", exception.getMessage());
    }

    @Test
    public void prim50000_outOfBounds_Test() {
        boolean[] prim50000 = PrimGenerator.generatePrimNumbers(43781);
        Exception exception = assertThrows(ArrayIndexOutOfBoundsException.class, () -> prim50000[10000] = true);
        assertEquals("Index 10000 out of bounds for length 10000", exception.getMessage());
    }
}
