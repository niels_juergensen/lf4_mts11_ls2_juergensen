package math;

public class MyMath {

    public static boolean isPrim(long zahl) {
        if (zahl == 1) {
            return false;
        }

        for (long i = 2; i < zahl; i++) {
            if (zahl % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static long roundDownToLast10000(long zahl){
        return zahl = zahl / 10000 * 10000;
    }
}
