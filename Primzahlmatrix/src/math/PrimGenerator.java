package math;

public class PrimGenerator {

    public static boolean[] generatePrimNumbers() {
        boolean[] primNumbers = new boolean[40000];

        for (int i = 0; i < primNumbers.length; i++) {
            primNumbers[i] = MyMath.isPrim(i + 1);
        }

        return primNumbers;
    }

    public static boolean[] generatePrimNumbers(long startzahl) {
        startzahl = MyMath.roundDownToLast10000(startzahl);

        boolean[] primNumbers = new boolean[40000];

        for (int i = 0; i < primNumbers.length; i++) {
            primNumbers[i] = MyMath.isPrim(startzahl + i + 1);
        }

        return primNumbers;
    }
}
