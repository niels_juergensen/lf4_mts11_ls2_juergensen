import java.awt.*;

public abstract class Figur {
    protected Color farbe;

    public abstract double umfang();

    public abstract double flaeche();

    public abstract String toString();
}