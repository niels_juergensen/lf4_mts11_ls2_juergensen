import java.awt.*;

public class Kreis extends Figur {
    private double radius;

    public Kreis(double radius, Color farbe) {
        this.radius = radius;
        this.farbe = farbe;
    }

    public Color getFarbe(){
        return farbe;
    }

    public void setFarbe(Color farbe){
        this.farbe = farbe;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double flaeche() {
        return Math.pow(radius, 2) * Math.PI;
    }

    @Override
    public double umfang() {
        return 2 * radius * Math.PI;
    }

    @Override
    public String toString() {
        return "Farbe: " + farbe +
                ", radius = " + radius +
                ", umfang = " + Math.floor(umfang() * 100) / 100 +
                ", flaeche = " + Math.floor(flaeche() * 100) / 100;
    }
}