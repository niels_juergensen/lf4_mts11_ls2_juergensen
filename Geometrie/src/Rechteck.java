import java.awt.*;

public class Rechteck extends Figur {
    private double seiteA, seiteB;

    public Rechteck(double seiteA, double seiteB, Color farbe) {
        this.seiteA = seiteA;
        this.seiteB = seiteB;
        this.farbe = farbe;
    }

    public Color getFarbe(){
        return farbe;
    }

    public void setFarbe(Color farbe){
        this.farbe = farbe;
    }

    public double getSeiteA() {
        return seiteA;
    }

    public void setSeiteA(double seiteA) {
        this.seiteA = seiteA;
    }

    public double getSeiteB() {
        return seiteB;
    }

    public void setSeiteB(double seiteB) {
        this.seiteB = seiteB;
    }

    @Override
    public double flaeche() {
        return seiteA * seiteB;
    }

    @Override
    public double umfang() {
        return 2 * seiteA + 2 * seiteB;
    }

    @Override
    public String toString(){
        return "Farbe: " + farbe +
                ", seiteA = " + seiteA +
                ", seiteB = " + seiteB +
                ", umfang = " + Math.floor(umfang() * 100) / 100 +
                ", flaeche = " + Math.floor(flaeche() * 100) / 100;
    }
}