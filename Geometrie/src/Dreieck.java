import java.awt.*;

public class Dreieck extends Figur {

    private double seiteA, seiteB, seiteC;

    public Dreieck(double seiteA, double seiteB, double seiteC, Color farbe) {
        this.seiteA = seiteA;
        this.seiteB = seiteB;
        this.seiteC = seiteC;
        this.farbe = farbe;
    }

    public Color getFarbe(){
        return farbe;
    }

    public void setFarbe(Color farbe){
        this.farbe = farbe;
    }

    public double getSeiteA() {
        return seiteA;
    }

    public void setSeiteA(double seiteA) {
        this.seiteA = seiteA;
    }

    public double getSeiteB() {
        return seiteB;
    }

    public void setSeiteB(double seiteB) {
        this.seiteB = seiteB;
    }

    public double getSeiteC() {
        return seiteC;
    }

    public void setSeiteC(double seiteC) {
        this.seiteC = seiteC;
    }

    public double alpha() {
        return Math.acos((Math.pow(seiteC, 2.0) + Math.pow(seiteB, 2.0) - Math.pow(seiteA, 2.0)) / (2.0 * seiteB * seiteC));
    }

    public double beta() {
        return Math.acos((Math.pow(seiteA, 2) + Math.pow(seiteC, 2) - Math.pow(seiteB, 2)) / (2 * seiteA * seiteC));
    }

    public double gamma() {
        return Math.acos((Math.pow(seiteA, 2) + Math.pow(seiteB, 2) - Math.pow(seiteC, 2)) / (2 * seiteA * seiteB));
    }

    public double hoeheA() {
        return seiteC * Math.sin(beta());
    }

    public double hoeheB() {
        return seiteC * Math.sin(alpha());
    }

    public double hoeheC() {
        return seiteA * Math.sin(beta());
    }

    @Override
    public double umfang() {
        return seiteA + seiteB + seiteC;
    }

    @Override
    public double flaeche() {
        return seiteC * hoeheC() / 2;
    }

    @Override
    public String toString() {
        return "Farbe: " + farbe +
                ", seiteA = " + seiteA +
                ", seiteB = " + seiteB +
                ", seiteC = " + seiteC +
                ", alpha = " + Math.floor(Math.toDegrees(alpha()) * 100) / 100 +
                ", beta = " + Math.floor(Math.toDegrees(beta()) * 100) / 100 +
                ", gamma = " + Math.floor(Math.toDegrees(gamma()) * 100) / 100 +
                ", hoeheA = " + Math.floor(hoeheA() * 100) / 100 +
                ", hoeheB = " + Math.floor(hoeheB() * 100) / 100 +
                ", hoeheC = " + Math.floor(hoeheC() * 100) / 100 +
                ", umfang = " + Math.floor(umfang() * 100) / 100 +
                ", flaeche = " + Math.floor(flaeche() * 100) / 100;
    }
}