// Die folgende Klasse besitzt einige Methoden, die getestet werden sollen.
// Überlegen Sie sich, welche mathematischen Besonderheiten gelten und testen Sie auch diese.
// Versuchen Sie mithilfe von Tests den Quellcode in fehlerfreieren Quellcode zu transformieren.

public class Bruch {
    private int zaehler;
    private int nenner;

    public int getZaehler() {
        return zaehler;
    }

    public void setZaehler(int zaehler) {
        this.zaehler = zaehler;
    }

    public int getNenner() {
        return nenner;
    }

    public void setNenner(int nenner) {
        if (nenner == 0) {
            throw new IllegalArgumentException();
        } else {
            this.nenner = nenner;
        }
    }

    public Bruch(int zaehler, int nenner) {
        this.setZaehler(zaehler);
        this.setNenner(nenner);
    }

    public void kuerze() {
        int tmpn = this.nenner;
        int tmpz = this.zaehler;
        if (tmpz != 0) {
            int rest;
            int ggt = Math.abs(tmpz);
            int divisor = Math.abs(tmpn);
            do {
                rest = ggt % divisor;
                ggt = divisor;
                divisor = rest;
            } while (rest > 0);
            tmpz = tmpz / ggt;
            tmpn = tmpn / ggt;
        }
        this.nenner = tmpn;
        this.zaehler = tmpz;
    }

    public static int ggT(int n, int m) {
        return n == m ? n : n < m ? ggT(n, m - n) : ggT(n - m, m);
    }

    public Bruch multipliziereMit(Bruch other) {
        int tmpz = zaehler * other.zaehler;
        int tmpn = nenner * other.nenner;
        return new Bruch(tmpz, tmpn);
    }

    public Bruch dividiereMit(Bruch other) {
        int tmpz = zaehler * other.nenner;
        int tmpn = nenner * other.zaehler;
        return new Bruch(tmpz, tmpn);
    }

    public Bruch addiereMit(Bruch other) {
        int tmpn = nenner * other.getNenner();
        int tmpz1 = zaehler * other.getNenner();
        int tmpz2 = other.zaehler * nenner;
        int tmpz = tmpz1 + tmpz2;
        return new Bruch(tmpz, tmpn);
    }

    public Bruch subtrahiereMit(Bruch other) {
        int tmpn = nenner * other.getNenner();
        int tmpz1 = zaehler * other.getNenner();
        int tmpz2 = other.zaehler * nenner;
        int tmpz = tmpz1 - tmpz2;
        return new Bruch(tmpz, tmpn);
    }

    public void erweitern(int number) {
        nenner = nenner * number;
        zaehler = zaehler * number;
    }
}