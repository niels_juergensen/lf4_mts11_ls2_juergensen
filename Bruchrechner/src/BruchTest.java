import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BruchTest {

    @Test
    public void When_SechsUndDreiAnKonstruktorGegeben_Then_BruchSechsDrittelErstellt() {
        Bruch testBruch = new Bruch(6, 3);
        assertEquals(6, testBruch.getZaehler());
        assertEquals(3, testBruch.getNenner());
    }

    @Test(expected = IllegalArgumentException.class)
    public void When_SechsUndNullAnKonstruktorGegeben_Then_IllegalArgumentExceptionWirdGeworfen() {
        Bruch testBruch = new Bruch(6, 0);
    }

    @Test
    public void When_KuerzeUngekuerzterBruch_Then_BruchWirdMaximalGekuerzt() {
        Bruch testBruch = new Bruch(9, 27);
        testBruch.kuerze();

        assertEquals(1, testBruch.getZaehler());
        assertEquals(3, testBruch.getNenner());
    }

    @Test
    public void When_KuerzeGekuerzterBruch_Then_BruchBleibtGleich() {
        Bruch testBruch = new Bruch(4, 9);
        testBruch.kuerze();

        assertEquals(4, testBruch.getZaehler());
        assertEquals(9, testBruch.getNenner());
    }

    @Test(expected = NullPointerException.class)
    public void When_BruchWirdMitNullMultipliziert_Then_NullPointerExceptionWirdGeworfen() {
        Bruch testBruch = new Bruch(4, 9);
        testBruch.multipliziereMit(null);
    }

    @Test
    public void When_BruchWirdMitAnderemBruchMultipliziert_Then_ZahlerUndNennerWerdenJeweilsMultipliziert() {
        Bruch testBruch = new Bruch(4, 9);
        Bruch testBruch2 = new Bruch(2, 3);

        Bruch testErgebnis = testBruch.multipliziereMit(testBruch2);

        assertEquals(8, testErgebnis.getZaehler());
        assertEquals(27, testErgebnis.getNenner());
    }

    @Test(expected = NullPointerException.class)
    public void When_BruchWirdMitNullDividierd_Then_NullPointerExceptionWirdGeworfen() {
        Bruch testBruch = new Bruch(4, 9);
        testBruch.dividiereMit(null);
    }

    @Test
    public void When_BruchWirdMitAnderemBruchDividierd_Then_ZahlerUndNennerWerdenUeberKreuzMultipliziert() {
        Bruch testBruch = new Bruch(4, 9);
        Bruch testBruch2 = new Bruch(2, 3);

        Bruch testErgebnis = testBruch.dividiereMit(testBruch2);

        assertEquals(12, testErgebnis.getZaehler());
        assertEquals(18, testErgebnis.getNenner());
    }
}
