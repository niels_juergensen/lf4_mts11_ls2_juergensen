import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Raumschiff klingonen = new Raumschiff(1, 100,
                100, 100, 100, "IKS Hegh'ta",
                2, new ArrayList<Ladung>());

        Raumschiff romulaner = new Raumschiff(2, 100,
                100, 100, 100, "IRW Khazara",
                2, new ArrayList<Ladung>());

        Raumschiff vulkanier = new Raumschiff(0, 80,
                80, 50, 100, "Ni'Var",
                5, new ArrayList<Ladung>());

        klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
        klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));

        romulaner.addLadung(new Ladung("Borg-Schrott", 5));
        romulaner.addLadung(new Ladung("Rote Materie", 2));
        romulaner.addLadung(new Ladung("Plasma-Waffe", 50));

        vulkanier.addLadung(new Ladung("Forschungssonde", 35));
        vulkanier.addLadung(new Ladung("Photonentorpedo", 3));


        klingonen.photonentorpedoAbschiessen(romulaner);
        romulaner.phaserkanoneAbschiessen(klingonen);

        vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");

        klingonen.printZustand();
        klingonen.printLadungsverzeichnis();

        vulkanier.reparaturAndroidenEinsetzen(vulkanier.getAndroidenAnzahl(), true,
                true, true);

        vulkanier.ladungPhotonentorpedosEinsetzen(3);
        vulkanier.ladungsverzeichnisAufraeumen();

        klingonen.photonentorpedoAbschiessen(romulaner);
        klingonen.photonentorpedoAbschiessen(romulaner);

        klingonen.printZustand();
        klingonen.printLadungsverzeichnis();

        romulaner.printZustand();
        romulaner.printLadungsverzeichnis();

        vulkanier.printZustand();
        vulkanier.printLadungsverzeichnis();

        System.out.println(Raumschiff.getLogbuchEintraege());
    }
}
