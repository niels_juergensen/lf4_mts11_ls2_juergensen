import java.util.ArrayList;

public class Raumschiff {
    private String schiffsname;
    private int energieversorgungInProzent, schildeInProzent, lebenserhaltungssystemeInProzent,
            huelleInProzent, photonentorpedoAnzahl, androidenAnzahl;
    private ArrayList<Ladung> ladungsverzeichnis;
    static ArrayList<String> broadcastKommunikator = new ArrayList<>();

    Raumschiff() {
        this(0, 100,
                100, 100, 100, "",
                0, new ArrayList<Ladung>());
    }

    Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
               int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffsname,
               int androidenAnzahl, ArrayList<Ladung> ladungsverzeichnis) {
        setSchiffsname(schiffsname);
        setEnergieversorgungInProzent(energieversorgungInProzent);
        setSchildeInProzent(schildeInProzent);
        setLebenserhaltungssystemeInProzent(lebenserhaltungssystemeInProzent);
        setHuelleInProzent(huelleInProzent);
        setPhotonentorpedoAnzahl(photonentorpedoAnzahl);
        setAndroidenAnzahl(androidenAnzahl);
        setLadungsverzeichnis(ladungsverzeichnis);
    }

    /**(Effekt.:) Gibt den Namen des Raumschiffs zurueck.
     * @return schiffsname */
    public String getSchiffsname() {
        return schiffsname;
    }

    /**(Effekt.:) Setzt den Namen des Raumschiffs als den der Methode uebergebenen Parameter.
     * @param schiffsname der neue Name des Raumschiffs */
    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }

    /**(Effekt.:) Gibt die Energieversorgung des Raumschiffs in Prozent zurueck.
     * @return energieversorgungInProzent */
    public int getEnergieversorgungInProzent() {
        return energieversorgungInProzent;
    }

    /**(Effekt.:) Setzt die Energieversorgung des Raumschiffs in Prozent als den der Methode uebergebenen Parameter.
     * Die Energieversorgung kann nicht kleiner als 0 oder groeßer als 100 werden.
     * @param energieversorgungInProzent die neue Energieversorgung des Raumschiffs in Prozent */
    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        if (energieversorgungInProzent < 0) {
            this.energieversorgungInProzent = 0;
        } else {
            if (energieversorgungInProzent > 100) {
                this.energieversorgungInProzent = 100;
            } else {
                this.energieversorgungInProzent = energieversorgungInProzent;
            }
        }
    }
    /**(Effekt.:) Gibt die Schilde des Raumschiffs in Prozent zurueck.
     * @return schildeInProzent */
    public int getSchildeInProzent() {
        return schildeInProzent;
    }


    /**(Effekt.:) Setzt die Schilde des Raumschiffs in Prozent als den der Methode uebergebenen Parameter.
     * * Die Schilde koennen nicht kleiner als 0 oder groeßer als 100 werden.
     * @param schildeInProzent die neue Energieversorgung des Raumschiffs in Prozent */
    public void setSchildeInProzent(int schildeInProzent) {
        if (schildeInProzent < 0) {
            this.schildeInProzent = 0;
        } else {
            if (schildeInProzent > 100) {
                this.schildeInProzent = 100;
            } else {
                this.schildeInProzent = schildeInProzent;
            }
        }
    }

    /**(Effekt.:) Gibt die Lebenserhaltungssysteme des Raumschiffs in Prozent zurueck.
     * @return lebenserhaltungssystemeInProzent */
    public int getLebenserhaltungssystemeInProzent() {
        return lebenserhaltungssystemeInProzent;
    }

    /**(Effekt.:) Setzt die Lebenserhaltungsysteme des Raumschiffs in Prozent als den der Methode uebergebenen Parameter.
     * * Die Lebenserhaltungssysteme koennen nicht kleiner als 0 oder groeßer als 100 werden.
     * @param lebenserhaltungssystemeInProzent die neue Energieversorgung des Raumschiffs in Prozent */
    public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
        if (lebenserhaltungssystemeInProzent < 0) {
            this.lebenserhaltungssystemeInProzent = 0;
        } else {
            if (lebenserhaltungssystemeInProzent > 100) {
                this.lebenserhaltungssystemeInProzent = 100;
            } else {
                this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
            }
        }
    }

    /**(Effekt.:) Gibt die Huelle des Raumschiffs in Prozent zurueck.
     * @return huelleInProzent */
    public int getHuelleInProzent() {
        return huelleInProzent;
    }

    /**(Effekt.:) Setzt die Huelle des Raumschiffs in Prozent als den der Methode uebergebenen Parameter.
     * * Die Huelle kann nicht kleiner als 0 oder groeßer als 100 werden.
     * @param huelleInProzent die neue Energieversorgung des Raumschiffs in Prozent */
    public void setHuelleInProzent(int huelleInProzent) {
        if (huelleInProzent < 0) {
            this.huelleInProzent = 0;
        } else {
            if (huelleInProzent > 100) {
                this.huelleInProzent = 100;
            } else {
                this.huelleInProzent = huelleInProzent;
            }
        }
    }

    /**(Effekt.:) Gibt die Anzahl der Photonentorpedos des Raumschiffs zurueck.
     * @return photonentorpedoAnzahl */
    public int getPhotonentorpedoAnzahl() {
        return photonentorpedoAnzahl;
    }

    /**(Effekt.:) Setzt die Anzahl der Phototentorpedos des Raumschiffs als den der Methode uebergebenen Parameter.
     * * Die Anzahl kann nicht kleiner als 0 werden.
     * @param photonentorpedoAnzahl die neue Anzahl der Phototentorpedos des Raumschiffs */
    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
        if (photonentorpedoAnzahl < 0) {
            this.photonentorpedoAnzahl = 0;
        } else {
            this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        }
    }

    /**(Effekt.:) Gibt die Anzahl der Androiden des Raumschiffs zurueck.
     * @return androidenAnzahl */
    public int getAndroidenAnzahl() {
        return androidenAnzahl;
    }

    /**(Effekt.:) Setzt die Anzahl der Androiden des Raumschiffs als den der Methode uebergebenen Parameter.
     * * Die Anzahl kann nicht kleiner als 0 werden.
     * @param androidenAnzahl die neue Anzahl der Androiden des Raumschiffs */
    public void setAndroidenAnzahl(int androidenAnzahl) {
        if (androidenAnzahl < 0) {
            this.androidenAnzahl = 0;
        } else {
            this.androidenAnzahl = androidenAnzahl;
        }
    }

    /**(Effekt.:) Gibt das Ladungsverzeichnis des Raumschiffs zurueck.
     * @return ladungsverzeichnis */
    public ArrayList<Ladung> getLadungsverzeichnis() {
        return ladungsverzeichnis;
    }

    /**(Effekt.:) Setzt das Ladungsverzeichnis des Raumschiffs als den der Methode uebergebenen Parameter.
     * @param ladungsverzeichnis das neue Ladungsverzeichnis des Raumschiffs */
    public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
        this.ladungsverzeichnis = ladungsverzeichnis;
    }

    /**(Effekt.:) Fügt dem Ladungsverzeichnis des Raumschiffs den der Methode uebergebenen Parameter hinzu.
     * Ueberprüft ob Ladung mit der Bezeichnungs der neuen Ladung schon vorhanden. Falls ja,
     * wird die vorhandene Menge um die neue erhöht. Falls nein, wird ein neuer Eintrag angelegt.
     * @param neueLadung die dem Ladungsverzeichnis des Raumschiffs hinzugefügte Ladung */
    public void addLadung(Ladung neueLadung) {
        boolean isLadungNeu = true;

        for (int i = 0; i < ladungsverzeichnis.size(); i++) {
            if (ladungsverzeichnis.get(i).getBezeichnung().equals(neueLadung.getBezeichnung())) {
                ladungsverzeichnis.get(i).setMenge(ladungsverzeichnis.get(i).getMenge() + neueLadung.getMenge());
                isLadungNeu = false;
                break;
            }
        }

        if (isLadungNeu) {
            ladungsverzeichnis.add(neueLadung);
        }
    }

    /**(Effekt.:) Gibt alle Schiffsname, Energieversorgung, Schilde, Lebenserhalung, Huelle
     * sowie Anzahl von Photonentorpedos, Androiden und Ladungen des Raumschiffs in der Konsole aus. */
    public void printZustand() {
        System.out.println("-------------Zustandsbericht-------------");
        System.out.println("Schiffsname: " + schiffsname);
        System.out.println("Energieversorgung in Prozent: " + energieversorgungInProzent);
        System.out.println("Schilde in Prozent: " + schildeInProzent);
        System.out.println("Lebenserhaltung in Prozent: " + lebenserhaltungssystemeInProzent);
        System.out.println("Huelle in Prozent: " + huelleInProzent);
        System.out.println("Anzahl von Photonentorpedos: " + photonentorpedoAnzahl);
        System.out.println("Anzahl von Androiden: " + androidenAnzahl);
        System.out.println("Anzahl von Ladungen: " + ladungsverzeichnis.size());
    }

    /**(Effekt.:) Gibt die Bezeichnung und Menge aller Ladungen des Raumschiffs in der Konsole aus. */
    public void printLadungsverzeichnis() {
        System.out.println("-------------Ladungsverzeichnis-------------");
        System.out.println("Schiffsname: " + schiffsname);

        for (int i = 0; i < ladungsverzeichnis.size(); i++) {
            System.out.println("Ladungsbezeichnung: " + ladungsverzeichnis.get(i).getBezeichnung() +
                    " | Menge: " + ladungsverzeichnis.get(i).getMenge());
        }
    }

    /**Voraussetzung.: Falls das Raumschiff Photonentorpedos geladen hat,
     * (Effekt.:) wird einer auf das der Methode übergebene Raumschiff geschossen.
     * Dieses Raumschiff ruft dann die Methode trefferVermerken auf.
     * Andernfalls wird die Nachricht "-=*Click*=-" an alle Raumschiffe gesendet.
     * @param ziel das Raumschiff das Ziel des Photonentorpedos ist */
    public void photonentorpedoAbschiessen(Raumschiff ziel) {
        if (photonentorpedoAnzahl < 1) {
            nachrichtAnAlle("-=*Click*=-");
        } else {
            photonentorpedoAnzahl -= 1;
            nachrichtAnAlle("Photonentorpedo abgeschossen");
            ziel.trefferVermerken();
        }
    }

    /**Voraussetzung.: Falls das Raumschiff genug Energie hat,
     * (Effekt.:) wird einer auf das der Methode übergebene Raumschiff geschossen.
     * Dieses Raumschiff ruft dann die Methode trefferVermerken auf.
     * Andernfalls wird die Nachricht "-=*Click*=-" an alle Raumschiffe gesendet.
     * @param ziel das Raumschiff das Ziel der Phaserkanone ist */
    public void phaserkanoneAbschiessen(Raumschiff ziel) {
        if (energieversorgungInProzent < 50) {
            broadcastKommunikator.add("-=*Click*=-");
        } else {
            setEnergieversorgungInProzent(energieversorgungInProzent - 50);
            nachrichtAnAlle("Phaserkanone abgeschossen");
            ziel.trefferVermerken();
        }
    }

    /**(Effekt.:) Das Raumschiff registriert einen Treffer und ermittelt den angerichteten Schaden.
     * Dabei wird zuerst der Schild beschaedigt, falls dieser zerstoert ist wirde die Huelle beschaedigt
     * und falls auch die Huelle zerstoert ist werden die Lebenserhaltungssysteme zerstoert. */
    private void trefferVermerken() {
        System.out.println(schiffsname + " wurde getroffen!");

        setSchildeInProzent(schildeInProzent - 50);

        if (schildeInProzent == 0) {
            setHuelleInProzent(huelleInProzent - 50);
            setEnergieversorgungInProzent(energieversorgungInProzent - 50);

            if (huelleInProzent == 0) {
                setLebenserhaltungssystemeInProzent(0);
                nachrichtAnAlle("Lebenserhaltungssysteme vernichtet");
            }
        }
    }

    /**(Effekt.:) Der der Methode übergebene Parameter wird als Nachricht an
     * alle Raumschiffe gesendet.
     * @param nachricht die Nachricht die an alle Raumschiffe gesendet wird */
    public void nachrichtAnAlle(String nachricht) {
        broadcastKommunikator.add(schiffsname + ": " + nachricht);
    }

    /**(Effekt.:) Gibt alle Nachrichten zurueck die das Raumschiffs empfangen hat.
     * @return broadcastKommunikator */
    public static ArrayList<String> getLogbuchEintraege() {
        return broadcastKommunikator;
    }

    /**(Effekt.:) Das Raumschiff versucht die der Methode als Parameter uebergebene Anzahl an Photonentorpedos
     * zu laden. Falls das Raumschiff in seiner Ladung nicht genug findet, werden alle vorhandenen geladen.
     * @param anzahl die Anzahl der Photonentorpedos die das Raumschiff versucht zu laden */
    public void ladungPhotonentorpedosEinsetzen(int anzahl) {
        int anzahlGeladen = 0;

        for (int i = 0; i < ladungsverzeichnis.size(); i++) {
            if (ladungsverzeichnis.get(i).getBezeichnung().equals("Photonentorpedo")) {
                if (ladungsverzeichnis.get(i).getMenge() >= anzahl) {
                    anzahlGeladen = anzahl;
                } else {
                    anzahlGeladen = ladungsverzeichnis.get(i).getMenge();
                }
                setPhotonentorpedoAnzahl(photonentorpedoAnzahl + anzahlGeladen);
                ladungsverzeichnis.get(i).setMenge(ladungsverzeichnis.get(i).getMenge() - anzahlGeladen);
                break;
            }
        }

        if (anzahlGeladen == 0) {
            nachrichtAnAlle("Keine Photonentorpedos gefunden!");
        } else {
            nachrichtAnAlle(anzahlGeladen + " Photonentorpedo(s) eingesetzt");
        }
    }

    /**(Effekt.:) Alle Eintraege im Ladungsverzeichnis bei denen die Menge gleich 0 ist werden gelöscht.
     * @return true, falls die Abhebung ausgeführt werden konnte, sonst false */
    public void ladungsverzeichnisAufraeumen() {
        for (int i = ladungsverzeichnis.size() - 1; i >= 0; i--) {
            if (ladungsverzeichnis.get(i).getMenge() == 0) {
                ladungsverzeichnis.remove(i);
            }
        }
    }

    /**(Effekt.:) Das Raumschiff versucht die der Methode als Parameter uebergebene Anzahl an Androiden
     * zur Reparatur zu verwenden. Falls das Raumschiff in seiner Ladung nicht Androiden hat, werden alle vorhandenen verwendet.
     * Die Systeme werden nur repariert, wenn der jeweilige für das System zustaendige Parameter beim Aufruf der Methode als true uebergeben wurde.
     * Der Reparatur Effekt ist groeßer umso weniger Systeme repariert werden und außerdem variant.
     * @param androidenAnzahl die Anzahl der Androiden die das Raumschiff versucht zu verwenden
     * @param schildeRepariert true, falls Schilde repariert werden sollen
     * @param energieversorgungRepariert true, falls Energieversorgung repariert werden soll
     * @param huelleRepariert true, falls Huelle repariert werden soll */
    public void reparaturAndroidenEinsetzen(int androidenAnzahl,
                                            boolean schildeRepariert,
                                            boolean energieversorgungRepariert,
                                            boolean huelleRepariert) {


        int reperaturErfolg = (int) Math.round(Math.random() * 100);
        int androidenEingesetzt, anzahlSystemeRepariert = 0, reparaturWert = 0;

        if (androidenAnzahl > this.androidenAnzahl) {
            androidenEingesetzt = this.androidenAnzahl;
        } else {
            androidenEingesetzt = androidenAnzahl;
        }

        if(schildeRepariert){
            anzahlSystemeRepariert++;
        }
        if(energieversorgungRepariert){
            anzahlSystemeRepariert++;
        }
        if(huelleRepariert){
            anzahlSystemeRepariert++;
        }

        if(anzahlSystemeRepariert>0){
            reparaturWert = reperaturErfolg * androidenEingesetzt / anzahlSystemeRepariert;
        }

        if(schildeRepariert){
            setSchildeInProzent(schildeInProzent+reparaturWert);
        }
        if(energieversorgungRepariert){
            setEnergieversorgungInProzent(energieversorgungInProzent+reparaturWert);
        }
        if(huelleRepariert){
            setHuelleInProzent(huelleInProzent+reparaturWert);
        }
    }
}