public class Ladung {
    private String bezeichnung;
    private int menge;

    Ladung() {
    }

    Ladung(String bezeichnung, int menge) {
        setBezeichnung(bezeichnung);
        setMenge(menge);
    }

    /**(Effekt.:) Gibt die Bezeichnung der Ladung zurueck.
     * @return bezeichnung */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**(Effekt.:) Setzt die Bezeichnung der Ladung als den der Methode uebergebenen Parameter.
     * @param bezeichnung die neue Bezeichnung der Ladung */
    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    /**(Effekt.:) Gibt die Menge der Ladung zurueck.
     * @return bezeichnung */
    public int getMenge() {
        return menge;
    }

    /**(Effekt.:) Setzt die Menge der Ladung als den der Methode uebergebenen Parameter.
     * @param menge die neue Menge der Ladung */
    public void setMenge(int menge) {
        if (menge < 0) {
            this.menge = 0;
        } else {
            this.menge = menge;
        }
    }

    /**(Effekt.:) Gibt den Java-Klassenname, die Bezeichnung und die Menge der Ladung als String zurueck.
     * @return "Klassenname Bezeichnung Menge" */
    @Override
    public String toString(){
        return this.getClass().getSimpleName()+" "+bezeichnung+" "+menge;
    }
}