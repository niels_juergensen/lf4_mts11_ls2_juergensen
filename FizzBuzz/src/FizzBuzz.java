public class FizzBuzz {

    public String fizzbuzz(int x){
        if(x % 5 == 0){
            if(x % 3 == 0){
                return "FizzBuzz";
            } else return "Buzz";
        } else {
            if(x % 3 == 0){
                return "Fizz";
            } else return String.valueOf(x);
        }
    }
}
