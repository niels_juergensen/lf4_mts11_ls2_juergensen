import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FizzBuzzTest {
    @Test
    public void When_xVielfachesVonDrei_Then_FizzWirdZurueckgegeben(){
        int x = 9;
        FizzBuzz fizzbuzz = new FizzBuzz();

        assertEquals("Fizz", fizzbuzz.fizzbuzz(x));
    }

    @Test
    public void When_xVielfachesVonFuenf_Then_BuzzWirdZurueckgegeben(){
        int x = 10;
        FizzBuzz fizzbuzz = new FizzBuzz();

        assertEquals("Buzz", fizzbuzz.fizzbuzz(x));
    }

    @Test
    public void When_xVielfachesVonFuenfUndDrei_Then_FizzBuzzWirdZurueckgegeben(){
        int x = 30;
        FizzBuzz fizzbuzz = new FizzBuzz();

        assertEquals("FizzBuzz", fizzbuzz.fizzbuzz(x));
    }

    @Test
    public void When_xNichtVielfachesVonFuenfOderDrei_Then_xWirdZurueckgegeben(){
        int x = 4;
        FizzBuzz fizzbuzz = new FizzBuzz();

        assertEquals(String.valueOf(x), fizzbuzz.fizzbuzz(x));
    }
}
